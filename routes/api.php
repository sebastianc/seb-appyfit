<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function ($request) {

    //Account management
    Route::group(['prefix' => 'auth'], function ($request) {
        Route::post('/login', 'API\V1\AuthController@login');
        Route::post('/register', 'API\V1\AuthController@register');
        Route::get('/logout', 'API\V1\AuthController@logout');
        Route::post('/reset', 'API\V1\AuthController@reset');
    });

    //AUTHENTICATED ROUTES
    Route::group(['middleware' => 'jwt'], function ($request) {

        //Api info
        Route::group(['prefix' => 'auth'], function ($request) {
            Route::get('/appdata', 'API\V1\AuthController@appdata');
        });

        //Workouts
        Route::group(['prefix' => 'workouts'], function ($request) {
            Route::get('/', 'API\V1\WorkoutController@viewAll');
            Route::get('/search', 'API\V1\WorkoutController@search');
            Route::get('/dashboard', 'API\V1\WorkoutController@dashboard');
            Route::get('/{workout_id}', 'API\V1\WorkoutController@get');
        });

        //Workout Fav
        Route::group(['prefix' => 'workout_favourites'], function ($request) {
            Route::post('/add/{workout_id}', 'API\V1\WorkoutController@favourite');
            Route::delete('/{workout_id}', 'API\V1\WorkoutController@removeFavourite');
            Route::get('/', 'API\V1\WorkoutController@viewFavourites');
        });

        //Products
        Route::group(['prefix' => 'products'], function ($request) {
            Route::get('/', 'API\V1\ProductController@viewAll');
            Route::get('/{product_id}', 'API\V1\ProductController@get');
            //Route::post('/add', 'API\V1\ProductController@add');
        });

        //Users
        Route::group(['prefix' => 'users'], function ($request) {
            Route::get('/', 'API\V1\UserController@getUsers');
            Route::get('/{id}', 'API\V1\UserController@getById');
            Route::post('/{id}', 'API\V1\UserController@editUser');
        });

    });

});