<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkoutStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workout_styles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('style_id')->unsigned()->default(1);
            $table->foreign('style_id')->references('id')->on('styles');
            $table->integer('workout_id')->unsigned()->default(1);
            $table->foreign('workout_id')->references('id')->on('workouts')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workout_lengths');
    }
}
