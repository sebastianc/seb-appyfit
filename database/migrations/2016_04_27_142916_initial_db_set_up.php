<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDbSetUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        Schema::create('equipments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        Schema::create('fitness_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        Schema::create('instructors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('bio')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('key_aims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->nullable();
            $table->timestamps();
        });
        Schema::create('lengths', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->string('ip');
            $table->boolean('used')->default(0);
            $table->timestamps();
        });
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->timestamps();
        });
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier')->nullable();
            $table->string('title');
            $table->integer('price')->unsigned();
            $table->boolean('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('styles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('forename');
            $table->string('surname');
            $table->string('gender')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('street')->nullable();
            $table->string('city')->nullable();
            $table->string('postcode')->nullable();
            $table->string('password', 60);
            $table->boolean('active');
            $table->string('type');
            $table->string('push_token')->nullable();
            $table->string('activation')->nullable();
            $table->integer('fitness_level_id')->unsigned()->default(1);
            $table->foreign('fitness_level_id')->references('id')->on('fitness_levels');
            $table->integer('key_aim_id')->unsigned()->default(1);
            $table->foreign('key_aim_id')->references('id')->on('key_aims');
            $table->timestamp('premium_expire')->nullable();
            $table->boolean('is_admin')->default(0);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->default(1);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->default(1);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('amount')->unsigned();
            $table->string('json')->nullable();
            $table->string('base_raw')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('verification_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->default(1);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('code')->nullable();
            $table->timestamps();
        });
        Schema::create('workouts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instructor')->unsigned()->default(1);
            $table->foreign('instructor')->references('id')->on('instructors')->onDelete('cascade');
            $table->string('name');
            $table->integer('fitness_level')->unsigned()->default(1);
            $table->foreign('fitness_level')->references('id')->on('fitness_levels');
            $table->integer('style')->unsigned()->default(1);
            $table->foreign('style')->references('id')->on('styles');
            $table->integer('key_aim')->unsigned()->default(1);
            $table->foreign('key_aim')->references('id')->on('key_aims');
            $table->integer('body_area')->unsigned()->default(1);
            $table->foreign('body_area')->references('id')->on('body_areas');
            $table->longText('synopsis')->nullable();
            $table->integer('length')->unsigned()->default(1);
            $table->foreign('length')->references('id')->on('lengths');
            $table->string('video_id')->nullable();
            $table->string('duration')->nullable();
            $table->string('publish_status');
            $table->string('type');
            $table->boolean('freecontent');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('workout_cooldowns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('workout_id')->unsigned()->default(1);
            $table->foreign('workout_id')->references('id')->on('workouts')->onDelete('cascade');
            $table->integer('cooldown_id')->unsigned()->default(1);
            $table->foreign('cooldown_id')->references('id')->on('workouts')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('workout_equipments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipment_id')->unsigned()->default(1);
            $table->foreign('equipment_id')->references('id')->on('equipments')->onDelete('cascade');
            $table->integer('workout_id')->unsigned()->default(1);
            $table->foreign('workout_id')->references('id')->on('workouts')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('workout_warmups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('workout_id')->unsigned()->default(1);
            $table->foreign('workout_id')->references('id')->on('workouts')->onDelete('cascade');
            $table->integer('warmup_id')->unsigned()->default(1);
            $table->foreign('warmup_id')->references('id')->on('workouts')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('favourites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('workout_id')->unsigned()->default(1);
            $table->foreign('workout_id')->references('id')->on('workouts')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->default(1);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('likes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('workout_id')->unsigned()->default(1);
            $table->foreign('workout_id')->references('id')->on('workouts')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->default(1);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('like')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_areas');
        Schema::dropIfExists('equipments');
        Schema::dropIfExists('favourites');
        Schema::dropIfExists('fitness_levels');
        Schema::dropIfExists('key_aims');
        Schema::dropIfExists('lengths');
        Schema::dropIfExists('instructors');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('posts');
        Schema::dropIfExists('products');
        Schema::dropIfExists('receipts');
        Schema::dropIfExists('styles');
        Schema::dropIfExists('verification_codes');
        Schema::dropIfExists('users');
        Schema::dropIfExists('workout_cooldowns');
        Schema::dropIfExists('workout_equipments');
        Schema::dropIfExists('workout_warmups');
        Schema::dropIfExists('workouts');
    }
}
