<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Sofa\Eloquence\Eloquence;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 40,
        'professional.total_bookings' => 12,
        'phone' => 20,
        'referral_code' => 15,
    ];

    /**
     * Hash all passwords before saving to DB
     * Old functionality
     */
    /*function setPasswordAttribute($raw){
        $this->attributes['password'] = Hash::make($raw);
    }*/

    /**
     * Reset a users password from the token.
     *
     * @param $token
     * @param $password
     * @return bool True if the password was reset
     */
    function usePasswordReset($token, $password)
    {
        $reset = PasswordReset
            ::where('token', $token)
            ->where('username', $this->username)
            ->where('used', false)
            ->first();

        if (!$reset) {
            return false;
        }

        $reset->used = true;
        $reset->save();
        $this->password = $password;

        return true;
    }

    /**
     * Create a verification code for the users phone.
     *
     * @return VerificationCode
     */
    public function createVerificationCode()
    {
        //Only allow one verification code at a time
        VerificationCode::where('user_id', $this->id)->delete();

        $verification = new VerificationCode();
        $verification->user_id = $this->id;
        $verification->code = mt_rand(10000, 99999);
        $verification->save();

        return $verification;
    }

    /**
     * Attempt to verify the users phone using a verification code
     *
     * @param string $code The code
     * @return bool True if the phone was verified
     */
    public function useVerificationCode($code)
    {
        $code = VerificationCode::where('user_id', $this->id)->where('code', $code)->first();

        if (!$code) {
            return false;
        }

        $code->delete();
        $this->phone_verified = true;

        return true;
    }

    /**
     * Route email notifications to the user
     */
    public function routeNotificationForEmail()
    {
        return $this->username;
    }

    /**
     * Route SMS notifications to the user
     */
    public function routeNotificationForTwilio()
    {
        return $this->mobile_phone;
    }

    /**
     * Set the users profile image
     *
     * @param Image $image
     */
    /*public function setProfileImageAttribute(Image $image)
    {
        $path = public_path('uploads');
        $name = str_random(32) . '.jpg';
        $image->save($path . '/' . $name);
        $this->attributes['profile_image'] = 'uploads/' . $name;
    }*/

    /**
     * Get the users profile image
     *
     * @param $value
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    /*public function getProfileImageAttribute($value)
    {
        return $value ? url($value) : null;
    }*/
}