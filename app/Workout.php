<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class Workout extends Model
{
    use SoftDeletes;

    protected $with = ['instructor', 'workout_equipment', 'warmups', 'cooldowns', 'styles'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'instructor', 'name', 'fitness_level', 'style', 'key_aim', 'body_area', 'synopsis', 'length', 'video_id', 'publish_status', 'type', 'freecontent'
    ];

    /**
     * Custom to array function
     * @return array
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 60,
        'instructor' => 60,
        'fitness_level' => 60,
        'style' => 60,
        'key_aim' => 60,
        'body_area' => 60,
        'synopsis' => 60,
        'video_id' => 60,
        'publish_status' => 60,
        'type' => 60,
        'freecontent' => 60,
    ];

    function instructor()
    {
        return $this->belongsTo('App\Instructor', 'instructor', 'id');
    }

    function workout_equipment()
    {
        return $this->hasMany('App\WorkoutEquipment');
    }

    public function warmups()
    {
        return $this->hasMany('App\WorkoutWarmup');
    }

    public function cooldowns()
    {
        return $this->hasMany('App\WorkoutCooldown');
    }

    public function styles()
    {
        return $this->hasMany('App\WorkoutStyle');
    }

}
