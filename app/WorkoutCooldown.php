<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class WorkoutCooldown extends Model
{
    use SoftDeletes;

    protected $with = ['cooldown'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workout_id', 'cooldown_id'
    ];

    /**
     * Custom to array function
     * @return array
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'workout_id' => 60,
        'cooldown_id' => 60,
    ];

    public function cooldown()
    {
        return $this->belongsTo('App\Workout', 'cooldown_id', 'id');
    }

    public function workout()
    {
        return $this->belongsTo('App\Workout', 'workout_id');
    }

}
