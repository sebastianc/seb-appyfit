<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 06/06/2017
 * Time: 08:28
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Receipt;
use App\Workout;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function viewAll()
    {
        $products = Product
            ::where('active', 1)
            ->orderBy('created_at', 'desc')->paginate(16);

        return view('web.pages.products', compact('products'));
    }

    public function newProduct(Request $request)
    {
        return view('web.pages.addProduct');
    }

    public function editProduct($id)
    {
        $product = Product::find($id);

        return view('web.pages.editProduct', compact('product'));
    }

    public function updateProduct(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'active' => 'required',
            'is_only_new_users' => 'required',
        ]);

        $product = Product::find($request->input('id'));

        $product->identifier = $request->input('identifier');
        $product->title = $request->input('title');
        $product->price = $request->input('price');

        if($request->input('active') =='on') {
            $product->active = true;
        } else {
            $product->active = false;
        }

        $product->save();

        $product = Product::find($request->input('id'));

        return view('web.pages.editProduct', compact('product'));

    }

    public function saveProduct(Request $request)
    {
        $this->validate($request, [
            'identifier' => 'required',
            'title' => 'required',
            'price' => 'required',
            //'usage_limit' => 'required',
            'active' => 'required',
        ]);

        $product = new Product();

        $product->identifier = $request->input('identifier');
        $product->title = $request->input('title');
        $product->price = $request->input('price');
        //$product->usage_limit = $request->input('usage_limit');
        if($request->input('active') =='on') {
            $product->active = true;
        } else {
            $product->active = false;
        }

        $product->save();

        $products = Product
            ::where('active', 1)
            ->orderBy('created_at', 'desc')->paginate(16);

        return view('web.pages.products', compact('products'));

    }

}