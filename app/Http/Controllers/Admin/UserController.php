<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Favourite;
use App\Customer;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    // Get Users
    public function getUsers(Request $request) {
        $users = \App\User::withTrashed()
                ->where('type', 'customer');
        if(isset($request['s'])){
            $searchText = $request['s'];
            $users = $users->where('name', 'LIKE', '%'.$searchText.'%')->orWhere('email', 'LIKE', '%'.$searchText.'%')
                ->with('location')->orderBy('created_at', 'desc')->paginate(15);
            if($users->count() === 0 ){
                return view('web.pages.users', ['users' => $users, 'noResults' => $searchText]);
            }
            else {
                return view('web.pages.users', ['users' => $users]);
            }
        }
        else{
            $users = $users->with('location')->orderBy('created_at', 'desc')->paginate(15);
            return view('web.pages.users', ['users' => $users]);
        }
    }

    // Edit Users
    public function editUser($userId) {
        $user = User::find($userId);

        $favourites = Favourite::where('user_id', $user->id )
            ->orderBy('created_at', 'desc')->paginate(15);

        return view('web.pages.editUserProfile', ['user' => $user, 'favourites' => $favourites]);
    }

    //Enable or Disable a User
    public function toggleUser($userId) {
        $user = \App\User::withTrashed()
                ->find($userId);

        if($user) {
            if (!$user->deleted_at) {
                $user->delete();
            } else {
                $user->restore();
            }
        }

        return redirect()->back();
    }

    // Update Users
    public function updateUser(Request $request, $userId){
        $user = User::find($userId);

        if (isset($request['fullName']) || isset($request['email'])){
            $user->name = $request['fullName'];
            $user->email = $request['email'];
            $user->save();
        }
        if( $request->hasFile('profile_image') ) {
            $imageName = $request->file('profile_image')->getClientOriginalName();
            $path = base_path() . '/public/uploads/images/';
            $request->file('profile_image')->move($path , $imageName);
            $user->profile_image = Image::make($path.$imageName);
            $user->save();
        }

        return redirect()->route('editUserProfile', $userId);

    }
}
