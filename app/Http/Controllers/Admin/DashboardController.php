<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 22/05/17
 * Time: 14:29
 */

namespace App\Http\Controllers\Admin;


use App\UserSearchable;
use App\Workout;
use App\Receipt;
use App\User;

class DashboardController
{

    function index(){
        $workouts = $this->workouts();
        $revenue = $this->revenue();
        $registrations = $this->registrations();
        return view('web.pages.home')->with(compact('workouts', 'revenue', 'registrations'));
    }

    function workouts(){

        $total = Workout::get()->count();
        $week = Workout::ThisWeek()->get()->count();
        $month = Workout::ThisMonth()->get()->count();

        $workouts = [
            'total' => $total,
            'week' => $week,
            'month' => $month,
            'percentage' => ($week/$month)*100
        ];

        return $workouts;
    }

    function revenue(){
        if($revenue = Receipt::first()){
            $stats = [
              'total' => number_format($revenue->total, 2),
              'week' => number_format($revenue->week, 2),
              'month' => number_format($revenue->month, 2),
              'percentage' => number_format(($revenue->week/$revenue->month)*100, 2),

            ];
        }else{
            $stats = [
                'total' => 0.00,
                'week' => 0.00,
                'month' => 0.00,
                'percentage' => 0.00,

            ];
        }

        return $stats;

    }

    function registrations(){

        $total = User::get()->count();
        $week = User::ThisWeek()->get()->count();
        $month = User::ThisMonth()->get()->count();

        $users = [
            'total' => $total,
            'week' => $week,
            'month' => $month,
            'percentage' => ($week/$month)*100
        ];

        return $users;
    }

}