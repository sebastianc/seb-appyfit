<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
//use App\Jobs\SendEmail;
use App\Workout;
use App\Instructor;
use App\UserSearchable;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

class WorkoutController extends Controller
{
    //use DispatchesJobs;

    // Get Workouts
    public function getWorkouts() {
        $workouts = Workout::orderBy('created_at', 'desc')->paginate(15);
        return view('web.pages.getWorkouts', ['workouts' => $workouts]);
    }

    // Edit Workout
    public function editWorkout($workoutId) {
        $workout = Workout::where('id', $workoutId)->first();
        return view('web.pages.editWorkout', ['workout' => $workout]);
    }


    // Update Workout
    public function updateWorkout(Request $request){

        $this->validate($request, [
            'id' => 'required',
            /*'fitness_level' => 'required',
            'style' => 'required',
            'body_area' => 'required',
            'length' => 'required',*/
        ]);

        $workout = Workout::where('id', $request->input('id'))->first();
        //$instructor = Instructor::where

        if ($workout){

            $workout->instructor = $request->input('instructor');
            $workout->name = $request->input('name');
            $workout->fitness_level = $request->input('fitness_level');
            $workout->style = $request->input('style');
            $workout->key_aim = $request->input('key_aim');
            $workout->body_area = $request->input('body_area');
            $workout->synopsis = $request->input('synopsis');
            $workout->length = $request->input('length');
            $workout->video_id = $request->input('video_id');
            $workout->publish_status = $request->input('publish_status');
            $workout->freecontent = $request->input('freecontent');

            $workout->save();

            return redirect()->route('editWorkout', $workout->id);
        }
        else{
            $newWorkout = new Workout();
            $newWorkout->instructor = $request->input('instructor');
            $newWorkout->name = $request->input('name');
            $newWorkout->fitness_level = $request->input('fitness_level');
            $newWorkout->style = $request->input('style');
            $newWorkout->key_aim = $request->input('key_aim');
            $newWorkout->body_area = $request->input('body_area');
            $newWorkout->synopsis = $request->input('synopsis');
            $newWorkout->length = $request->input('length');
            $newWorkout->video_id = $request->input('video_id');
            $newWorkout->publish_status = $request->input('publish_status');
            $newWorkout->freecontent = $request->input('freecontent');
            $newWorkout->save();

            //dispatch(new SendEmail('subject', 'emails.updateWorkoutsuccess', $newWorkout, $newWorkout));

            return redirect()->route('editWorkout', $newWorkout->id);
        }

    }


}
