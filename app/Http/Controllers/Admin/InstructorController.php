<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Workout;
use App\BodyArea;
use App\Instructor;
use App\Product;
use App\KeyAim;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class InstructorController extends Controller
{
    // Get Instructors
    public function getInstructors(Request $request) {
        $instructors = Instructor::all();
        if(isset($request['s'])){
            $searchText = $request['s'];
            $instructors = $instructors->where('name', 'LIKE', '%'.$searchText.'%')->orWhere('email', 'LIKE', '%'.$searchText.'%')
                ->with('location')->orderBy('created_at', 'desc')->paginate(15);
            if($instructors->count() === 0 ){
                return view('web.pages.instructors', ['instructors' => $instructors, 'noResults' => $searchText]);
            }
            else{
                return view('web.pages.instructors', ['instructors' => $instructors]);
            }
        }
        else{
            $instructors = $instructors->with('location')->orderBy('created_at', 'desc')->paginate(15);
            return view('web.pages.instructors', ['instructors' => $instructors]);
        }
    }

    // Edit Instructors
    public function editInstructor($userId) {
        $user = User::where('id', $userId)->first();
        $instructor = Instructor::where('user_id', $userId)->first();
        if ($instructor) {
            $workouts = Workout::where('instructor', $instructor->id)
                ->orderBy('created_at', 'desc')->paginate(15);
        }
        else{
            $workouts = 'null';
            $total = 0;
        }
        return view('web.pages.editInstructorProfile', ['instructor' => $user, 'workouts' => $workouts,]);
    }

    // Update Instructors
    public function updateInstructor(Request $request, $instructorId){
        $instructor = User::find($instructorId);

        if (isset($request['fullName']) || isset($request['email']) || isset($request['location'])){
            $instructor->name = $request['fullName'];
            $instructor->email = $request['email'];
            $instructor->save();
        }
        if( $request->hasFile('profile_image') ) {
            $imageName = $request->file('profile_image')->getClientOriginalName();
            $path = base_path() . '/public/uploads/images/';
            $request->file('profile_image')->move($path , $imageName);
            $instructor->profile_image = Image::make($path.$imageName);
            $instructor->save();
        }

        return redirect()->route('editInstructorProfile', $instructorId);

    }

}
