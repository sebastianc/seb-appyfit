<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 25/03/17
 * Time: 10:13
 */

namespace App\Http\Controllers\API\V1;


use App\Workout;
use App\UserSearchable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class UserController extends ApiController
{

    /*public function getWorkouts($id) {

        $products = Workout::orderBy('created_at', 'desc')->orderBy('name', 'asc')->paginate(10);

        return parent::api_response($products, true, ['return' => 'workouts of user with id '.$id], 200);
    }*/

    public function getUsers()
    {
        $users = UserSearchable
            ::orderBy('is_admin', 'desc')->orderBy('created_at', 'desc')->orderBy('surname', 'asc')->paginate(15);
        return parent::api_response($users, true, ['return' => 'all users']);
    }

    function getById($id)
    {
        //$user = UserSearchable::with('follows', 'followers')->find($id);
        $user = UserSearchable::find($id);
        if ($user) {
            return parent::api_response($user, true, ['return' => 'User with id ' . $id], 200);
        } else {
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    public function getMonthly($id)
    {
        $info = Input::only('months');
        $monthsAgo = Carbon::today()->subMonths($info['months']);
        //$workouts = Workouts::where('id', '=', $id)->orderBy('created_at', 'desc')->get();
        $workouts = Workout::where('workout', '=', $id)->where(DB::raw('date(created_at)'), '>', $monthsAgo)->orderBy('created_at', 'desc')->get();
        $numberOfWorkouts = count($workouts);
        $avg = [];
        foreach ($workouts as $workout) {
            $workout['rating'] = preg_replace('/[^0-9.]+/', '', $workout['duration']);
            $avg += $workout['rating'];
        }

        $avg[] = $avg / $numberOfWorkouts;

        return parent::api_response($avg, true, ['return' => 'Workout rating for the last ' . $info['months'] . ' months'], 200);
    }

    public function editUser($id, Request $request)
    {
        $user = UserSearchable::find($id);
        $data = Input::only('username', 'password', 'forename', 'surname', 'gender', 'mobile_phone', 'street', 'city', 'type', 'postcode','fitness_level_id', 'key_aim_id');

        if ($user) {
            /* Validation rules when updating a user */
            $validator = Validator::make($request->all(), [
                'username' => 'string|unique:users|max:255',
                'password' => 'string|min:8',
                'forename' => 'string|min:2|max:42',
                'surname' => 'string|min:2|max:42',
                'type' => 'min:2|max:191',
                'fitness_level_id' => 'required|integer',
                'key_aim_id' => 'required|integer'
            ]);

            if ($validator->fails()) {
                return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
            } else {
                $user->username = $data['username'];
                $user->password = bcrypt($data['password']);
                $user->forename = $data['forename'];
                $user->surname = $data['surname'];
                $user->mobile_phone = $data['mobile_phone'];
                $user->street = $data['street'];
                $user->city = $data['city'];
                $user->gender = $data['gender'];
                $user->postcode = $data['postcode'];
                if(!empty($data['type'])) {
                    $user->type = $data['type'];
                }
                $user->fitness_level_id = $data['fitness_level_id'];
                $user->key_aim_id = $data['key_aim_id'];

                if ($user->save()) {
                    $request = new Request();
                    $request->merge($data);
                    return parent::api_response($user, true, ['return' => 'Successfully updated User Profile with id ' . $id], 200);
                }
            }
            // In case shake hand fails leave it empty for suspected bruteforce
        }
        else {
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

}