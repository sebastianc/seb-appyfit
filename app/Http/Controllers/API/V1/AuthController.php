<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 18/04/17
 * Time: 16:51
 */

namespace App\Http\Controllers\API\V1;

use App\User;
use App\Workout;
use App\BodyArea;
use App\Equipment;
use App\FitnessLevel;
use App\KeyAim;
use App\Length;
use App\Instructor;
use App\Product;
use App\UserSearchable;
use App\Style;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;


class AuthController extends ApiController

{
    use AuthenticatesUsers;

    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 600;


    /**
     * Log in a user.
     *
     * @param Request $request
     * @return json
     */
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        //Limit the amount of times users can login
        if ($this->hasTooManyLoginAttempts($request)) {
            return parent::api_response([], false, 'too many authentication attempts, try again in '.$this->lockoutTime.' seconds', 401);
        }

        $tempUser = User::where('username', $credentials['username'])
            ->where('active', 1)
            ->first();

        $token = JWTAuth::attempt($credentials, ['role' => $tempUser['type']]);

        //If the login attempt failed
        if (!$token) {
            $this->incrementLoginAttempts($request);
            return parent::api_response([], false, 'invalid credentials', 401);
        }

        $user = User::findOrFail(Auth::user()->id);

        return parent::api_response([
            'token' => $token,
            'user' => $user
        ],
            true,
            'User with username '.$user['username'].' is now Logged in');
    }

    /**
     * Register new user
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $data = Input::only('username', 'password', 'forename', 'surname', 'type', 'gender', 'mobile_phone', 'street', 'city', 'postcode', 'shake_hand');

        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users|max:255',
            'password' => 'required|min:8',
            'forename' => 'min:2|max:42',
            'surname' => 'min:2|max:42',
            'type' => 'min:2|max:191',
            //'premium' => 'required'
            'shake_hand' => 'required'
        ]);
        if ($validator->fails()) {
            return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
        } elseif($data['shake_hand']=='deltacharileecho') {
            $new_user = new User;
            $new_user->username = $data['username'];
            $new_user->password = bcrypt($data['password']);
            if(!empty($data['forename'])) {
                $new_user->forename = $data['forename'];
            } else {
                $new_user->forename = '';
            }
            if(!empty($data['surname'])) {
                $new_user->surname = $data['surname'];
            } else {
                $new_user->surname = '';
            }
            $new_user->active = true;
            if(!empty($data['type'])) {
                $new_user->type = $data['type'];
            } else {
                $new_user->type = 'subscriber';
            }
            $new_user->fitness_level_id = 1;
            $new_user->key_aim_id = 1;
            if(!empty($data['premium'])) {
                $new_user->premium = $data['premium'];
                $new_user->premium_expire = $data['premium_expire'];
            }

            if($new_user->save()) {
                $request = new Request();
                $request->merge($data);
                return $this->login($request);
            }
        }
        // In case shake hand fails leave it empty for suspected bruteforce
    }

    /**
     * Logout user
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request){
        if(!Auth::logout()){
            $data = Input::only('token');
            $validator = Validator::make($request->all(), [
                'token' => 'required|max:355'
            ]);
            if (!$validator->fails()) {
                if ($data['token']) {
                    JWTAuth::invalidate($data['token']);
                }
            } else {
                return parent::api_response([], false, ['Error' => 'user not found'], 200);
            }
            return $this->api_response([], true, ['Success' => 'logged out'], 200);
        }
    }

    /**
     * Reset password
     * @return mixed
     */
    public function reset(){
        $username = stripslashes(Input::get('username'));
        $user = User::where('username', $username)->first();

        if($user){
            $raw_pass = str_random(12);
            $new_pass = bcrypt($raw_pass);
            $user->password = $new_pass;
            $data['user'] = $user->toArray();
            $data['password'] = $raw_pass;
            if($user->parse_id && !$user->initial_pass_reset){
                $user->initial_pass_reset = 1;
            }
            if($user->save()){
                Mail::send('emails.API.V1.reset', ['data' => $data], function ($m) use ($username, $data) {
                    $m->from('hey@appyfit.uk', 'AppyFit');

                    $m->to($username, 'user')->subject('Pass Reset');
                });
                return parent::api_response([], true, ['Success' => 'new pass sent'], 200);
            }

        }else{
            return parent::api_response([], false, ['Error' => 'user not found'], 404);
        }

    }

    public function appdata(Request $request) {

        $data = Input::only('shake_hand');

        $validator = Validator::make($request->all(), [
            'shake_hand' => 'required'
        ]);

        if ($validator->fails()) {
            return parent::api_response([], false, ['error' => $validator->errors()->first()], 404);
        } elseif($data['shake_hand']=='tangotangoecho') {

            $workouts = Workout::orderby('instructor')->orderby('name')->get();
            $body_areas = BodyArea::orderby('id')->get();
            $instructors = Instructor::orderby('id')->get();
            $fitness_levels = FitnessLevel::orderby('id')->get();
            $key_aims = KeyAim::orderby('id')->get();
            $lengths = Length::orderby('id')->get();
            $equipments = Equipment::orderby('id')->get();
            $products = Product::all();
            //$users = UserSearchable::orderby('surname')->get();
            //$admins = UserSearchable::orderby('surname')->where('is_admin', 1)->get();
            $styles = Style::orderby('id')->get();

            $data = [
                'body_areas' => $body_areas,
                'instructors' => $instructors,
                'fitness_levels' => $fitness_levels,
                'key_aims' => $key_aims,
                'lengths' => $lengths,
                'equipments' => $equipments,
                'products' => $products,
                //'users' => $users,
                //'admins' => $admins,
                'styles' => $styles,
                'workouts' => $workouts
            ];

            return parent::api_response($data, true, ['Success' => 'Appdata'], 200);
        } else {
            return parent::api_response([], false, ['error' => 'Access Denied'], 401);
        }
    }

    public function update(Request $request)
    {
        $app = $request->input('app');
        $appVersion = config('appyfit.version.'. $app);

        if ($appVersion == null) {
            return parent::api_response([], false, ['error' => 'invalid app parameter'], 200);
        }

        $shouldUpdate = version_compare($appVersion, $request->input('version'), '>');

        $data = [
            'should_update' => $shouldUpdate
        ];

        return parent::api_response($data, true, ['Success' => 'update'], 200);
    }


}