<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 20/04/17
 * Time: 16:35
 */

namespace App\Http\Controllers\API\V1;

use App\Product;
use App\UserSearchable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends ApiController
{
    function viewAll(){

        $products = Product
            ::orderBy('created_at', 'desc')
            ->get();

        return parent::api_response($products, true, ['return' => 'All IAP Products'], 200);
    }

    function get($id){
        $product = Product::find($id);
        if($product) {
            return parent::api_response($product, true, ['return' => 'Selected Product with id '.$id], 200);
        } else {
            return parent::api_response([], false, ['error' => 'Product not found'], 404);
        }
    }

}