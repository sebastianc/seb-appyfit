<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 21/04/17
 * Time: 16:35
 */

namespace App\Http\Controllers\API\V1;

use App\Workout;
use App\Favourite;
use App\Product;
use App\UserSearchable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;

class WorkoutController extends ApiController
{
    function viewAll(){

        $workouts = Workout
            ::with('instructor', 'workout_equipment')
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return parent::api_response($workouts, true, ['return' => 'All Workouts'], 200);
    }

    function get($id){
        $workout = Workout::find($id);
        if($workout) {
            return parent::api_response($workout, true, ['return' => 'Workout with id '.$id], 200);
        } else {
            return parent::api_response([], false, ['error' => 'Workout not found'], 404);
        }
    }

    /**
     * Add Workout to favs
     * @param Request $request
     * @return mixed
     */
    public function favourite($id)
    {
        $workout = Workout::find($id);
        if($workout){
            $favourite = Favourite::where('workout_id', $id)
                ->where('user_id', Auth::user()->id)
                ->get();
            if(count($favourite)) {
                return parent::api_response([], false, ['error' => 'Workout already favourited'], 400);
            } else {
                $new_fav = new Favourite;
                $new_fav->workout_id = $id;
                $new_fav->user_id = Auth::user()->id;

                if($new_fav->save()) {
                    return parent::api_response($new_fav, true, ['return' => 'Successfully added Workout with id '.$id. ' to Favourites'], 200);
                } else {
                    return parent::api_response([], false, ['error' => 'Failed to add workout '.$id. ' to Favourites'], 500);
                }
            }

        }else{
            return parent::api_response([], false, ['error' => 'Workout with id '.$id.' not found'], 404);
        }
    }

    /**
     * Remove Workout from favs
     * @param Request $request
     * @return mixed
     */
    public function removeFavourite($id)
    {
        $workout = Workout::find($id);
        if($workout){
            $favourite = Favourite::where('workout_id', $id)
                ->where('user_id', Auth::user()->id)
                ->first();
            if(count($favourite)) {
                if ($favourite->delete()) {
                    return parent::api_response($workout, true, ['success' => 'Successfully removed Workout with id '.$id.' from Favourites'], 200);
                } else {
                    return parent::api_response($workout, true, ['error' => 'Error removing Workout from favourites'], 500);
                }
            } else {
                return parent::api_response([], false, ['error' => 'You have not favourited that Workout or it was already removed from favourites'], 400);
            }

        }else{
            return parent::api_response([], false, ['error' => 'Workout with id '.$id.' not found'], 404);
        }
    }

    function viewFavourites(){
        $favourites = Favourite
            ::with('workout', 'user')
            ->where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        if(count($favourites)) {
            return parent::api_response($favourites, true, ['return' => 'All Favourites by User with id '.Auth::user()->id], 200);
        } else {
            return parent::api_response([], false, ['return' => 'No Favourites at the moment'], 404);
        }

    }

    /**
     * Search Workouts by fitness level
     * @param Request $request
     * @return mixed
     */

    function search(Request $request){

        $data = Input::only('fitness_level', 'page');

        $currentPage = $data['page'];

        /*Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });*/

        Paginator::currentPageResolver(function () use ($currentPage) {
            return Input::get("page", $currentPage);
        });

        $workouts = Workout
            ::with('instructor', 'workout_equipment')
            ->where('fitness_level', $data['fitness_level'])
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $workouts->appends( Input::only('fitness_level') );

        if(count($workouts)) {
            return parent::api_response($workouts, true, ['return' => 'Page '.$currentPage.' of All Workouts with Fitness Level id of '.$data['fitness_level']], 200);
        } else {
            return parent::api_response([], false, ['return' => 'No Workouts with Fitness Level id of '.$data['fitness_level']], 404);
        }
    }

    /**
     * Search Workouts by fitness level
     * @param Request $request
     * @return mixed
     */

    function dashboard(){

        $features = DB::table('workouts')
            ->select('workouts.id', 'workouts.name', 'workouts.synopsis', 'workouts.length',  'workouts.type', 'workouts.video_id', 'workouts.freecontent')
            ->limit(6)
            ->inRandomOrder()
            ->get();

        $favourites = DB::table('favourites')
            //->select('favourites.workout_id')
            //->distinct()
            ->pluck('workout_id')
            ->all();

        //$favourites = collect($favourites)->toArray();

        $workoutFavs = DB::table('workouts')
            ->whereIn('workouts.id', $favourites)
            ->select('workouts.id', 'workouts.name', 'workouts.synopsis', 'workouts.length',  'workouts.type', 'workouts.video_id', 'workouts.freecontent')
            ->get();

        $strength = DB::table('workouts')
            ->where('workouts.key_aim', 4)
            ->select('workouts.id', 'workouts.name', 'workouts.synopsis', 'workouts.length',  'workouts.type', 'workouts.video_id', 'workouts.freecontent')
            ->get();

        $result = [
            'dashTitle' => 'Featured Videos',
            'features' => $features,
            'sections' => [
                [
                    'id' => "9d42e564-6bbc-4a3e-9d6d-e7b6a4a84919",
                    //'name' => 'Recommended for you',
                    'name' => 'Other users liked',
                    'workouts' => $workoutFavs

                ],
                [
                    'id' => 'e9430e39-3a75-4475-bbb4-db3cae974f4f',
                    'name' => "Build Strength",
                    'workouts' => $strength

                ]
            ]
        ];

        return parent::api_response($result, true, ['return' => 'Dashboard Page'], 200);

    }

}