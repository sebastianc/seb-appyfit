<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class WorkoutStyle extends Model
{
    use SoftDeletes;

    protected $with = ['style'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'style_id', 'workout_id'
    ];

    /**
     * Custom to array function
     * @return array
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'style_id' => 60,
        'workout_id' => 60,
    ];

    public function style()
    {
        return $this->belongsTo('App\Style', 'style_id', 'id');
    }

    public function workout()
    {
        return $this->belongsTo('App\Workout', 'workout_id', 'id');
    }

}
