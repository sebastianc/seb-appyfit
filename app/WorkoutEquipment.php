<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class WorkoutEquipment extends Model
{
    use SoftDeletes;

    protected $with = ['equipment'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workout_id', 'equipment_id'
    ];

    /**
     * Custom to array function
     * @return array
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'workout_id' => 60,
        'equipment_id' => 60,
    ];

    public function equipment()
    {
        return $this->belongsTo('App\Equipment', 'equipment_id', 'id');
    }

    public function workout()
    {
        return $this->belongsTo('App\Workout', 'workout_id');
    }

}
