<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class FitnessLevel extends Model
{

    //protected $with = ['workout_equipment'];

    /**
     * Custom to array function
     * @return array
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 40,
    ];

}
