<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 11/04/2017
 * Time: 10:05
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class UserSearchable extends Model{

    protected $table = 'users';
    /**
     * Relations to be loaded by default.
     *
     * @var array
     */
    /*protected $with = [
        'listings'
    ];*/
    protected $dates = ['deleted_at', 'updated_at'];

    use SoftDeletes;
    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 25,
        'email' => 35,
    ];


    function toArray(){

        $array = parent::toArray();
        return $array;
    }


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function getEmail(){
        return $this->email;
    }

    /**
     * Future possible functionality: Workouts the user is doing something with
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    /*public function workouts()
    {
        return $this->hasMany('App\Workout', 'seller')->orderBy('created_at', 'desc');
    }*/

}
