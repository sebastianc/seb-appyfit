<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/php; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AppyFit Web Dashboard</title>

        <!-- Bootstrap -->
        <link href="{{ asset('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset('assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="{{ asset('assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
        <!-- bootstrap-switch -->
        <link href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet">
        <!-- Star Rating -->
        <link href="{{ asset('assets/css/star-rating.css') }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">
    </head>

    <body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <!-- side menu -->
            @include('web.includes.sidemenu')
            <!-- /side menu -->

            <!-- top navigation -->
            @include('web.includes.header')
            <!-- /top navigation -->

            <!-- page content -->
            @yield('content')
            <!-- /page content -->

            <!-- footer content -->
            @include('web.includes.footer')
            <!-- /footer content -->
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Bootstrap Switch -->
    <script src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <!-- Bootstrap datepicker -->
    <script src="{{ asset('assets/vendors/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/js/datepicker/daterangepicker.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('assets/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('assets/vendors/nprogress/nprogress.js') }}"></script>
    <!-- Chart.js -->
    <script src="{{ asset('assets/vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('assets/vendors/iCheck/icheck.min.js') }}"></script>
    <!-- Flot -->
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.resize.js') }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('assets/js/flot/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('assets/js/flot/date.js') }}"></script>
    <script src="{{ asset('assets/js/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('assets/js/flot/curvedLines.js') }}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('assets/js/star-rating.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.tablesorter.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.tablesorter.pager.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    @yield('scripts')

    </body>
    </html>