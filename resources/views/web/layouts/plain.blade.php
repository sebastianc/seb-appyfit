<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/php; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AppyFit Web Panel</title>

        <!-- Bootstrap -->
        <link href="{{ URL::to('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ URL::to('assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ URL::to('assets/css/custom.css') }}" rel="stylesheet">
    </head>

    <body class="nav-md white-bg">
    <div class="container body">
        <div class="main_container">

            <!-- page content -->
            @yield('content')
            <!-- /page content -->

        </div>
    </div>
    </body>
    </html>