
<div class="container-fluid">
    <div class="row">
        <div class="reset-div main-box">
            <div class="col-xs-12 visible-xs no-padding relative">
            </div>
            <aside class="col-sm-4 col-xs-12 light-grey-bg">
                <h3 class="mint text-center col-sm-12 margin-bottom-1 black hidden-xs">Reset password</h3>
                <form class="col-sm-12 margin-top-1">
                    <p class="dark-grey col-xs-12 text-center margin-top-1 help-block ">Please enter the new password</p>
                    <fieldset class="col-xs-12">
                        <div class="form-group">
                            <input class="form-control" type="password" placeholder="New password" required>
                        </div>
                        <div class="form-group">
                            <a href="" class="btn mint-btn white col-sm-12 col-xs-12">Reset</a>
                        </div>
                    </fieldset>
                </form>
            </aside>
        </div>
    </div>
</div>
