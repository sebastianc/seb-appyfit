
</div>
<!-- End page content -->
<!-- Footer -->
<footer class="light-grey-bg col-xs-12 no-padding">
    <ul class="social col-sm-6 col-xs-6">
        <li><a href="//www.instagram.com/" target="_blank"> <img src="/assets/images/icons/instagram.png" alt=""></a></li>
        <li><a href="//twitter.com/" target="_blank"> <img src="/assets/images/icons/tt.png" alt=""></a></li>
        <li><a href="//www.facebook.com/" target="_blank"> <img src="/assets/images/icons/fb.png" alt=""></a></li>
    </ul>
    <ul class=" col-sm-6 col-xs-6 visible-xxs">
        <li><a href="" class="dark-grey">Terms</a></li>
        <li><a href="" class="dark-grey">Privacy Policy</a></li>
        <li class="light-grey">&copy; <?php echo date("Y"); ?> AppyFit | All rights reserved</li>
    </ul>
    <ul class=" col-sm-6 col-xs-6 hidden-xxs">
        <li class="light-grey">&copy; <?php echo date("Y"); ?> AppyFit | All rights reserved</li>
        <li><a href="" class="dark-grey">Privacy Policy</a></li>
        <li><a href="" class="dark-grey">Terms</a></li>
    </ul>
</footer>
<!-- End Footer -->
<!-- JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/dropzone.min.js"></script>
<script src="assets/js/lightslider.min.js"></script>
<script src="assets/js/custom_jQuery.js"></script>
</body>
</html>
