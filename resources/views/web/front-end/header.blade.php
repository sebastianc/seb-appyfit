<!doctype html>
<html lang="en" class="">
<head>
    <meta charset="utf-8">
    <title>AppyFit</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/front-end/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/front-end/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/front-end/lightslider.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/front-end/dropzone.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/front-end/default.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/front-end/responsive.css')}}" />
</head>

<body>
<?php echo "<h1></h1>"; ?>
<header class="container-fluid">
    <!-- Navigation -->
    <nav class="navbar navbar-fixed-top">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{--<h4 class="navbar-brand"><span class="welcome ">Welcome</span><a href="#" class="logo "><img src="assets/images/logos/logo_white.png" alt="" class="img-responsive"></a></h4>--}}
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right ">
                <li><a id="register" href="#">Register</a></li>
                <li><a id="pro" href="#">Instructors</a></li>
                <li><a id="clientele" href="#">Users</a></li>
                <li><a id="download" href="#">Download</a></li>
            </ul>
            <ul class="social col-xs-12 visible-xs">
                <li><a href="//www.instagram.com/" target="_blank"> <img src="assets/images/icons/instagram_white.png" alt=""></a></li>
                <li><a href="//twitter.com/" target="_blank"> <img src="assets/images/icons/tt_white.png" alt=""></a></li>
                <li><a href="//m.facebook.com/" target="_blank"> <img src="assets/images/icons/fb_white.png" alt=""></a></li>
                <li class="light-grey col-xs-12">&copy; <?php echo date("Y"); ?> AppyFit - All rights reserved</li>
            </ul>
        </div><!--/.nav-collapse -->

    </nav>
    <!-- End Navigation -->
</header>

<!-- Popups -->


<!-- Page Content -->
<div class="page-wrapper">