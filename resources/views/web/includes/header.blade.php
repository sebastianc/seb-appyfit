<div class="top_nav">
    <div class="nav_menu bg-black">
        <nav class="" role="navigation">
            <div class="nav toggle hidden"> <a id="menu_toggle"><i class="fa fa-bars"></i></a> </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/logout" class="btn bg-grey logout">Log Out</a> </li>
                <li> <a href="javascript:;" class="user-profile"> <img src="{{ URL::to('assets/images/user.png') }}" alt="">{{Auth::guard('admin')->user()->name}}</a> </li>
            </ul>
        </nav>
    </div>
</div>