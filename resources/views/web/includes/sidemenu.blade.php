
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;"> <a href="index.php" class="site_title"><img src="{{ URL::to('assets/images/logo.png') }}" alt="" class="img-responsive"></a> </div>
        <div class="clearfix"></div>
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu col-sm-12">
            <div class="menu_section">
                <h3 class="text-uppercase">Main</h3>
                <ul class="nav side-menu">
                    <li><a href="{{ route('home') }}"><i class="fa fa-home"></i> Dashboard </a></li>
                    <li><a href="{{ route('users') }}"><i class="fa fa-users"></i> Users</a></li>
                    <li><a href="{{ route('instructors') }}"><i class="fa fa-cog"></i> Professionals</a></li>
                    <li><a href="{{ route('products') }}"><i class="fa fa-hashtag"></i> Products</a></li>
                    <li><a href="{{ route('workouts') }}"><i class="fa fa-edit"></i> Workouts</a></li>
                </ul>
            </div>
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li><a href="//www.appyfit.uk/" target="_blank"><i class="fa fa-laptop"></i> Admin Page </a> </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>