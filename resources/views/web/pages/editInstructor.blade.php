@extends('web.layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title bg-white col-xs-12">
            <div class="title_left">
                <h3 class="col-xs-12">Instructors</h3>
            </div>
            <div class="title_right">
                <ul class="breadcrumb pull-right">
                    <li><a href="">Instructors</a></li>
                    <li><a href="">Instructor's Profile</a></li>
                </ul>
            </div>
        </div>
        <div class="x_panel">
            <div class="page-title page-title-inner">
                <div class="title_left">
                    <h3> {{$instructor->name}} </h3>
                </div>
                <div class="title_right"></div>
            </div>
            <div class="x_content">
                <form role="form" class="form-inline" action="{{ route('updateInstructor', $instructor->id) }}" method="post" enctype="multipart/form-data">
                    <button class="btn btn-purple-in pull-right margin-left submit-btn" type="submit">Save Changes</button>
                    <h4 class="grey-font col-sm-12">Account</h4>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="activate">Active account</label>
                            <input type="checkbox" class="form-control switch" id="activate" name="activate">
                        </div>
                    </div>
                    <div class="col-sm-12 height1 border-bottom"></div>
                    <h4 class="grey-font col-sm-12">Personal Details</h4>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="fullName">Full Name</label>
                            <input type="text" class="form-control" id="fullName" name="fullName" value="{{ $instructor->name }}" >
                        </div>
                        <div class="form-group">
                            <label for="password">Image</label>
                            <input type="file" class="form-control" id="profile_image" name="profile_image" value="{{ $instructor->profile_image }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $instructor->email }}" >
                        </div>
                        <div class="form-group">
                            <label for="location">Location</label>
                            {{--<input type="text" class="form-control" id="location" name="location" value="{{ $instructor->location->name }}" >--}}
                            <select class="form-control" id="location" name="location">
                                @foreach($locations as $location)
                                    <option
                                    @if($location->id == $instructor->location->id)
                                            selected
                                    @endif
                                    >{{ $location->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="email">Total workouts</label>
                            <span>{{$total}}</span>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
            </div>
        </div>
    </div>
@endsection
