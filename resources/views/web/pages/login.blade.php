@extends('web.layouts.plain')

@section('content')
    <div class="container-fluid">
        <main role="main">
            <section class="login">
                <article class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 text-center">
                    <div class="img-holder">
                        <img src="{{ URL::to('assets/images/logo-small.png') }}" alt="" class="img-responsive">
                    </div>
                    <h4 class="purple ">Log in</h4>
                    <h3 class="text-capitalize text-center col-sm-12"></h3>
                    <form>
                        <!-- In case of error please add class " has-error" in the following div -->
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email Address">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                            <a href="" class="col-sm-12">*Reset password</a>
                        </div>
                        <input type="hidden" name="_token" value="">
                    </form>
                </article>
                <a type="submit" class="btn bt col-xs-12 white btn-purple-in" >Log in</a>
            </section>
        </main>
    </div>
@endsection
