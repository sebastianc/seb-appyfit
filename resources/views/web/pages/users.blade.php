@extends('web.layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title bg-white col-xs-12">
            <div class="title_left">
                <h3 class="col-xs-12">Users</h3>
            </div>
            <div class="title_right">
                <ul class="breadcrumb pull-right">
                    <li><a href="">Users</a></li>
                    <li><a href="">All Users</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="page-title page-title-inner">
                <div class="title_left">
                    <h3> All Users </h3>
                    <form class="col-md-7 col-sm-7 col-xs-12 form-group pull-left top_search"  action="{{ route('users') }}" method="get">
                        <div class="input-group">
                            <input type="text" name="s" class="form-control" placeholder="Search by name or email">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                            {{--<input type="hidden" name="_token" value="{{ Session::token() }}">--}}
                            </span>
                        </div>
                    </form>
                </div>
                <div class="title_right"> </div>
            </div>
            <div class="x_content">
                @if(isset($noResults))
                    <section class="text-center">
                        <h3>No results found for "{{ $noResults }}".</h3>
                    </section>
                @else
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Full Name </th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Email Address </th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Location </th>
                        <th width="100"></th>
                        <th width="100"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td><a href="{{ route('editUserProfile', $user->id) }}" class="user-profile"> <img src="{{ $user->profile_image }}" alt=""></a></td>
                            <td><a href="{{ route('editUserProfile', $user->id) }}">{{ $user->name }}</a></td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->location->name }}</td>
                            <td><a href="{{ route('toggleUser', $user->id) }}" class="btn btn-purple-in">
                                @if(!$user->deleted_at)
                                    Disable User
                                @else
                                    Enable User
                                @endif
                                </a></td>
                            <td><a href="{{ route('editUserProfile', $user->id) }}" class="btn btn-purple-in">View Profile</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
        <!-- pagination -->
        <div class="col-sm-12">
            {{ $users->links() }}
        </div>
    </div>
@endsection
