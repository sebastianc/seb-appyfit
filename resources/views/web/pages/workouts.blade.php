@extends('web.layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title bg-white col-xs-12">
            <div class="title_left">
                <h3 class="col-xs-12">Workouts</h3>
            </div>
            <div class="title_right">
                <ul class="breadcrumb pull-right">
                    <li><a href="">Workouts</a></li>
                    <li><a href="">All Workouts</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="page-title page-title-inner">
                <div class="title_left">
                    <h3> All Workouts </h3>
                    <form class="col-md-7 col-sm-7 col-xs-12 form-group pull-left top_search"  action="{{ route('workouts') }}" method="get">
                        <div class="input-group">
                            <input type="text" name="s" class="form-control" placeholder="Search by name or email">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                            {{--<input type="hidden" name="_token" value="{{ Session::token() }}">--}}
                            </span>
                        </div>
                    </form>
                </div>
                <div class="title_right"> </div>
            </div>
            <div class="x_content">
                @if(isset($noResults))
                    <section class="text-center">
                        <h3>No results found for "{{ $noResults }}".</h3>
                    </section>
                @else
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Workout Name </th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Fitness Level </th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Style </th>
                        <th width="100"></th>
                        <th width="100"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($workouts as $workout)
                        <tr>
                            <td><a href="{{ route('editWorkout', $workout->id) }}" class="workout-profile"> <img src="{{-- $workout->path --}}" alt=""></a></td>
                            <td><a href="{{ route('editWorkout', $workout->id) }}">{{ $workout->name }}</a></td>
                            <td>{{ $workout->fitness_level }}</td>
                            <td>{{ $workout->style }}</td>
                            <td><a href="{{ route('toggleWorkout', $workout->id) }}" class="btn btn-purple-in">
                                @if(!$workout->deleted_at)
                                    Disable User
                                @else
                                    Enable User
                                @endif
                                </a></td>
                            <td><a href="{{ route('editWorkout', $workout->id) }}" class="btn btn-purple-in">View Profile</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
        <!-- pagination -->
        <div class="col-sm-12">
            {{ $workouts->links() }}
        </div>
    </div>
@endsection
