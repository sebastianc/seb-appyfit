@extends('web.layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title bg-white col-xs-12">
            <div class="title_left">
                <h3 class="col-xs-12">Instructors</h3>
            </div>
            <div class="title_right">
                <ul class="breadcrumb pull-right">
                    <li><a href="">Instructors</a></li>
                    <li><a href="">All Instructors</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="page-title page-title-inner">
                <div class="title_left">
                    <h3> All Instructors </h3>
                    <form class="col-md-7 col-sm-7 col-xs-12 form-group pull-left top_search"  action="{{ route('instructors') }}" method="get">
                        <div class="input-group">
                            <input type="text" name="s" class="form-control" placeholder="Search by name or email">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                                {{--<input type="hidden" name="_token" value="{{ Session::token() }}">--}}
                            </span>
                        </div>
                    </form>
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="x_content">
                @if(isset($noResults))
                    <section class="text-center">
                        <h3>No results found for "{{ $noResults }}".</h3>
                    </section>
                @else
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Full Name </th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Email Address </th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Location </th>
                        <th><i class="fa fa-sort pull-left" aria-hidden="true"></i>Start Rating </th>
                        <th width="100"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($instructors as $instructor)
                        <tr>
                            <td><a href="{{ route('editInstructorProfile', $instructor->id) }}" class="user-profile"> <img src="{{ $instructor->profile_image }}" alt=""></a></td>
                            <td><a href="{{ route('editInstructorProfile', $instructor->id) }}">{{ $instructor->name }}</a></td>
                            <td>{{ $instructor->email }}</td>
                            <td>{{ $instructor->location->name }}</td>
                            <td data-rate="3">
                                <div class="rating-container rating-s rating-animate rating-disabled"><input value="{{ $instructor->rating }}" type="number" class="rating hide rating-loading" data-size="s" disabled="disabled"></div>
                            </td>
                            <td><a href="{{ route('editInstructorProfile', $instructor->id) }}" class="btn btn-purple-in">View Profile</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
        <!-- pagination -->
        <div class="col-sm-12">
            {{ $instructors->links() }}
        </div>
    </div>
@endsection
