@extends('web.layouts.master')

@section('content')
<div class="right_col bg-white" role="main">
    <h2 class="col-sm-12">AppyFit Web Dashboard</h2>
    <div class="col-sm-12">
        <!-- top tiles -->
        <div class="row tile_count">
            <div class="col-sm-3 col-xs-6 ">
                <div class="tile_stats_count"> <span class="count_top"> Total Users </span>
                    <div class="count"><span>This Week </span> <strong>{{$total_users['week']}}</strong></div>
                    <div class="count"><span>This Month </span> <strong>{{$total_users['month']}}</strong></div>
                    <div class="count"><span>Total </span> <strong>{{$total_users['total']}}</strong></div>
                    <div class="progress progress_sm">
                        <div class="progress-bar bg-orange" role="progressbar" data-transitiongoal="{{$total_users['percentage']}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 ">
                <div class="tile_stats_count"> <span class="count_top"> Total Registrations </span>
                    <div class="count"><span>This Week </span> <strong>{{$registrations['week']}}</strong></div>
                    <div class="count"><span>This Month </span> <strong>{{$registrations['month']}}</strong></div>
                    <div class="count"><span>Total </span> <strong>{{$registrations['total']}}</strong></div>
                    <div class="progress progress_sm">
                        <div class="progress-bar bg-orange" role="progressbar" data-transitiongoal="{{$registrations['percentage']}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="tile_stats_count"> <span class="count_top"> Total Workouts </span>
                    <div class="count"><span>This Week </span> <strong>£{{$workouts['week']}}</strong></div>
                    <div class="count"><span>This Month </span> <strong>£{{$workouts['month']}}</strong></div>
                    <div class="count"><span>Total </span> <strong>£{{$workouts['total']}}</strong></div>
                    <div class="progress progress_sm">
                        <div class="progress-bar bg-orange" role="progressbar" data-transitiongoal="{{$workouts['percentage']}}"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /top tiles -->

    </div>
</div>
@endsection

@section('scripts')
    <!-- Flot -->
    <script>
        $(document).ready(function() {
            var data1 = [
                [gd(2012, 1, 1), 17],
                [gd(2012, 1, 2), 74],
                [gd(2012, 1, 3), 6],
                [gd(2012, 1, 4), 39],
                [gd(2012, 1, 5), 20],
                [gd(2012, 1, 6), 85],
                [gd(2012, 1, 7), 7]
            ];
            $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
                data1
            ], {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0,
                        lineWidth: 3,
                        fill: 0
                    },
                    points: {
                        radius: 2,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    verticalLines: true,
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",
                    borderWidth: 1,
                    color: '#fff'
                },
                colors: ["#2e83e0"],
                xaxis: {
                    tickColor: "rgba(51, 51, 51, 0.06)",
                    mode: "time",
                    tickSize: [1, "day"],
                    //tickLength: 10,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                },
                yaxis: {
                    ticks: 8,
                    tickColor: "rgba(51, 51, 51, 0.06)",
                },
                tooltip: false
            });

            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }
        });
    </script>
    <!-- /Flot -->
@endsection