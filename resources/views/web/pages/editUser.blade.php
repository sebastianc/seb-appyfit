@extends('web.layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title bg-white col-xs-12">
            <div class="title_left">
                <h3 class="col-xs-12">Users</h3>
            </div>
            <div class="title_right">
                <ul class="breadcrumb pull-right">
                    <li><a href="">Users</a></li>
                    <li><a href="">User's Profile</a></li>
                </ul>
            </div>
        </div>
        <div class="x_panel">
            <div class="page-title page-title-inner">
                <div class="title_left">
                    <h3> All Users </h3>
                </div>
                <div class="title_right"> </div>
            </div>
            <div class="x_content">
                <form role="form" class="form-inline" action="{{ route('updateUserProfile', $user->id) }}" method="post" enctype="multipart/form-data">
                    <button class="btn btn-purple-in pull-right margin-left submit-btn" type="submit" >Save Changes</button>
                    <h4 class="grey-font col-sm-12">Account</h4>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="activate">Active account</label>
                            <input type="checkbox" class="form-control switch" id="activate" name="activate" checked>
                        </div>
                    </div>
                    <div class="col-sm-12 height1 border-bottom"></div>
                    <h4 class="grey-font col-sm-12">Personal Details</h4>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="fullName">Full Name</label>
                            <input type="text" class="form-control" id="fullName" name="fullName" value="{{ $user->name }}" >
                        </div>
                        <div class="form-group">
                            <label for="password">Image</label>
                            <input type="file" class="form-control" id="profile_image" name="profile_image" src="{{ $user->profile_image }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" >
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
                <div class="col-sm-12 height1 border-bottom"></div>
                @if ($favorites !== 'null')
                <div class="col-xs-12">
                    <h4 class="grey-font">Workout Favourites</h4>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><i class="fa fa-sort pull-left" aria-hidden="true"></i> Workout Name</th>
                            <th><i class="fa fa-sort pull-left" aria-hidden="true"></i> Style</th>
                            <th><i class="fa fa-sort pull-left" aria-hidden="true"></i> Key Aim</th>
                            <th><i class="fa fa-sort pull-left" aria-hidden="true"></i> Body Area</th>
                            <th><i class="fa fa-sort pull-left" aria-hidden="true"></i> Video Id</th>
                            <th><i class="fa fa-sort pull-left" aria-hidden="true"></i> Publish Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($favorites as $favorite)
                        <tr>
                            <td><a href="{{ route('editInstructor', $favorite->instructor->user->id) }}">{{ $favorite->name }}</a></td>
                            <td>{{ $favorite->style }}</td>
                            <td>{{ $favorite->key_aim }}</td>
                            <td width="200">{{ $favorite->body_area }}</td>
                            <td width="200">{{ $favorite->video_id }}</td>
                            <td width="200">£{{ $favorite->publish_status }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- pagination -->
                    <ul class="pagination col-sm-12" id="myPager"></ul>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection
