
<div class="container-fluid">
    <div class="row">
        <div class="reset-div main-box">
            <div class="col-xs-12 visible-xs no-padding relative">
                <p class="dark-grey col-xs-12 text-center margin-top-1 help-block ">Your password was updated.</p>
            </div>
        </div>
    </div>
</div>